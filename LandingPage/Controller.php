<?php
namespace LandingPage;

class Controller {
    public function chooseAction(\LandMvcHero\LandingPage $model)
    {
        $action = $model->getLandingAction();
        if (isset($_GET['action'])) {
            $action = $_GET['action'];
            $action = ($action === 'register' ? 3 : ($action === 'login' ? 2 : $action));
        } elseif (isset($_POST['submit'])) {
            switch ($_POST['submit']) {
                case 'Sign in':
                    $action = 2;
                    break;
                case 'Register':
                    $action = 3;
                    break;
                default:
                    $action = 1;
            }
        }

        return $model->setLandingAction($action);
    }

    public function handAction(\LandMvcHero\LandingPage $model)
    {
        $state = [];
        switch ($_POST['submit']) {
            case 'Sign in':
                $called_from = 'log';
                $state['called_from'] = $called_from;
                break;
            case 'Register':
                $called_from = 'reg';
                if ($model->ifUserExist($_POST['email'])) {
                    $state = [
                        'called_from' => $called_from,
                        'state' => 'error',
                        'state_message' => ': This email has been registered already. Please contact administrator if you forgot password or create user with another email'
                    ];
                    return $model->setActionState($state);
                }
                break;
            default:
                return 'Error in cntr handAction!';
        }

        $state = $this->filterValidateData($model, $called_from);

        $state_is_ready = ($state['state'] == 'ready');
        if ($state['called_from'] == 'reg' && $state_is_ready) {
            $state = $model->insertUser();
        } elseif ($state['called_from'] == 'log' && $state_is_ready) {
            // Try to log user
            if ($model->signIn($_POST['email'], $_POST['pass'])) {
                $state = [
                    'called_from' => $called_from,
                    'state' => 'success',
                    'state_message' => ': Please wait few seconds for registration to finish'
                ];
            } else {
                $state = [
                    'called_from' => $called_from,
                    'state' => 'error',
                    'state_message' => ': Password or email is incorrect' . ' ' . $_POST['email'] . ' ' .  $_POST['pass']
                ];
            }
        }

        return $model->setActionState($state);
    }

    private function filterValidateData($model, $called_from)
    {
        // Construct common validation method for each input field
        $filed_names_and_options = [
            'email' => [FILTER_SANITIZE_EMAIL, array('options' => ['min_range' => 0])],
            'pass' => [FILTER_VALIDATE_REGEXP, array('options' => ['regexp'=>'/^[\w]{1,7}[\w]$/'])]
        ];
        // Construct register specific validation method
        if ($called_from == 'reg') {
            $filed_names_and_options = [
                'f_name' => [FILTER_VALIDATE_REGEXP, array('options' => ["regexp"=>"/^[a-z]{1,15}[a-z]$/i"])], // ??? do I need to check if field is empty
                'l_name' => [FILTER_VALIDATE_REGEXP, array('options' => ["regexp"=>"/^[a-z]{1,15}[a-z]$/i"])],
            ];

            // Check if password fields match
            if ($_POST['pass'] != $_POST['repeat_pass']) {
                return [
                        'called_from' => $called_from,
                        'state' => 'error',
                        'state_message' => ': Password fields should match'
                ];
            }
        }

        // Validate user input data
        foreach ($filed_names_and_options as $filed_name=>$two_filter_options) {
            // Check if any fields were left empty and return error if yes
            if (empty($_POST[$filed_name])) {
                return [
                    'called_from' => $called_from,
                    'state' => 'error',
                    'state_message' => ': No fields should be left empty'
                ];
            }

            // Check if input data was right format
            $_POST[$filed_name] = filter_var($_POST[$filed_name], $two_filter_options[0], $two_filter_options[1]);
            if (!$_POST[$filed_name]) {
                return [
                    'called_from' => $called_from,
                    'state' => 'error',
                    'state_message' => ': Make sure all fields have valid input data'
                ];
            }
        }

        // Hash password
        $_POST['pass'] = $this->hashPass($_POST['pass']);
        // Return that input data is filled in right way
        $state_message = '';
        if ($called_from == 'reg') {
            $state_message = ': Wait for registration to finnish(success message will appear when done)';

        } elseif ($called_from == 'log') {
            $state_message = ': Wait for sign in process to finnish(success message will appear when done)';
        }

        return [
            'called_from' => $called_from,
            'state' => 'ready',
            'state_message' => $state_message
        ];
    }

    private function hashPass($pass): string
    {
        return md5($pass.'salt');
    }
}

//??? Questions: 1