<?php
namespace LandingPage;

class View {
    public function output(\LandMvcHero\LandingPage $model): string
    {
        switch ($active_link = $model->getLandingAction()) {
            case 1:
                $content = $this->land();
                break;
            case 2:
                $content = $this->login($model);
                break;
            case 3:
                $content = $this->register($model);
                break;
            default:
                $content = "Error with active_link";
        }

        $output = '<div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">';

        $output .= '<header class="mb-auto">
                        <div>
                            <h3 class="float-md-start mb-0">MVC Login</h3>
                            <nav class="nav nav-masthead justify-content-center float-md-end">
                                <a class="nav-link ' . ($active_link == 1 ? 'active' : '') . '" href="/">Land</a>
                                <a class="nav-link ' . ($active_link == 2 ? 'active' : '') . '" href="/?action=login">Login</a>
                                <a class="nav-link ' . ($active_link == 3 ? 'active' : '') . '" href="/?action=register">Register</a>
                            </nav>
                        </div>
                    </header>';


        $output .= $content;

        $output .= '<footer class="mt-auto text-white-50">
                        <p>©' . date("d.m.Y") . ' by <a href="https://bitbucket.org/quiteair/first_php_test_app/src/master/" class="text-white">@QuiteAir</a>.</p>
                    </footer>
                </div>';
        return $output;
    }

    private function land(): string
    {
        return '<main class="px-3">
                    <h1>Hello World.</h1>
                    <p class="lead">This is Ramil second try for building Login & Register page. Except this time it is done through MVC model.</p>
                    <p class="lead">
                        <a href="/?action=login" class="btn btn-lg btn-secondary fw-bold border-white bg-white">Login</a>
                    </p>
                </main>';
    }

    private function login($model): string
    {
        $result = '<div class="form-width m-auto">
                    <form action="/" method="post">
                        <div class="mb-3">
                            <label for="inputEmail" class="form-label">Email address</label>
                            <input type="email" name="email" value="' . $_SESSION["email"] . '" id="inputEmail" class="form-control">
                        </div>
                        <div class="mb-3">
                            <label for="inputPassword" class="form-label">Password</label>
                            <input type="password" name="pass" id="inputPassword" class="form-control">
                        </div>
                            <input type="submit" name="submit" value="Sign in"  class="btn btn-success">';
        if (!empty($model->getActionState())) {
            $result .= '<div id="submitHelp" class="error form-text">' . $model->getActionState()['state'] . $model->getActionState()['state_message'] . '</div>';
        }

        $result .= '</form>
                   </div>';
        return $result;
    }

    private function register($model): string
    {
        $result = '<div class="form-width m-auto">
                    <form action="/?action=register" method="post" class="card-body">
                        <div class="mb-3">
                            <label for="inputFN" class="form-label">First name</label>
                            <input type="text" name="f_name" value="' . (isset($_POST['f_name']) ? $_POST['f_name'] : '') . '" id="inputFN" class="form-control">
                        </div>
                        <div class="mb-3">
                            <label for="inputLN" class="form-label">Last name</label>
                            <input type="text" name="l_name" value="' . (isset($_POST['l_name']) ? $_POST['l_name'] : '') . '" id="inputLN" class="form-control">
                        </div>
                        <div class="mb-3">
                            <label for="inputEmail" class="form-label">Email address</label>
                            <input type="email" name="email" value="' . (isset($_POST['email']) ? $_POST['email'] : '') . '" id="inputEmail" class="form-control" aria-describedby="emailHelp">
                            <div id="emailHelp" class="form-text">We\'ll never share your email with anyone else.</div>
                        </div>
                        <div class="mb-3">
                            <label for="inputPassword" class="form-label">Password</label>
                            <input type="password" name="pass" id="inputPassword" class="form-control" placeholder="Password">
                            <input type="password" name="repeat_pass" id="repeatPassword" class="mt-2 form-control" placeholder="Repeat password">
                        </div>
                        <div>
                            <input type="submit" name="submit" value="Register"  class="btn btn-success" aria-describedby="submitHelp">';

        if (!empty($model->getActionState())) {
            $result .= '<div id="submitHelp" class="error form-text">' . $model->getActionState()['state'] . $model->getActionState()['state_message'] . '</div>';
        }

        $result .= '</div>
                </form>
            </div>';

        return $result;
    }
}