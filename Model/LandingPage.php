<?php
namespace LandMvcHero;

class LandingPage {
    private $pdo;
    private $landing_action;
    private $action_state;

    public function __construct(\PDO $pdo, $landing_action = 1, $fields_state = [])
    {
        $this->pdo = $pdo;
        $this->landing_action = $landing_action;
        $this->action_state = $fields_state;
    }

    public function getLandingAction(): int
    {
        return $this->landing_action;
    }

    public function getActionState(): array
    {
        return $this->action_state;
    }

    public function setLandingAction($new_landing_action): LandingPage
    {
        return new self($this->pdo, $new_landing_action, $this->action_state);
    }

    public function setActionState($action_state): LandingPage
    {
        return new self($this->pdo, $this->landing_action, $action_state);
    }

    public function ifUserExist($email): bool
    {
        $stmt = $this->pdo->prepare('SELECT * FROM users WHERE email = ?;');
        $stmt->execute([$email]);
        $stmt = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        if (count($stmt) > 0) {
            return true;
        }
        return false;
    }

    public function insertUser(): array
    {
        $stmt = $this->pdo->prepare('INSERT INTO users (f_name, l_name, email, password) VALUES (?, ?, ?, ?);');
        $stmt->execute(array($_POST['f_name'], $_POST['l_name'], $_POST['email'], $_POST['pass']));

        return ['state' => 'success', 'state_message' => ': you\'ve been successfully registered! You can register another user or login.'];
    }

    public function signIn($email, $pass): bool
    {
        $stmt = $this->pdo->prepare('SELECT id FROM users WHERE email = ? AND password = ?;');
        if ($stmt->execute(array($email, $pass))) {
            $stmt = $stmt->fetch(\PDO::FETCH_ASSOC);
            $_SESSION['logged'] = $stmt['id'];
            return true;
        }
        else {
            return false;
        }

    }
}