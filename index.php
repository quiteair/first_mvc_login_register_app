<!--fix same email registration error message-->

<?php
require 'Model/LandingPage.php';
require 'LandingPage/View.php';
require 'LandingPage/Controller.php';

require './config.php';
try {
    $pdo = new \PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_SCHEMA, DB_USER, DB_PASS);
} catch (PDOException $e) {
    print "Error!: " . $e->getMessage();
    die();
}

session_start();
?>

<!doctype html>
<html lang="en" class="h-100">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="styles.css">
    <title>MVC-hero</title>
</head>
<body class="d-flex h-100 text-center text-white bg-dark">
    <?php
        $model = new \LandMvcHero\LandingPage($pdo);
        $view = new \LandingPage\View();

        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            $controller = new \LandingPage\Controller();
            $model = $controller->chooseAction($model);
        } elseif ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $controller = new \LandingPage\Controller();
            $model = $controller->handAction($model);
            $model = $controller->chooseAction($model);
        }
        echo $view->output($model);
    ?>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>
<?php
?>